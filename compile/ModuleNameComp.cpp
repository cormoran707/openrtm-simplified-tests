#include <rtm/Manager.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include "ModuleName.h"


void MyModuleInit(RTC::Manager* manager)
{
  ModuleNameInit(manager);
  RTC::RtcBase* comp;

  // Create a component
  comp = manager->createComponent("ModuleName");

  if (comp==NULL)
  {
    std::cerr << "Component create failed." << std::endl;
    abort();
  }

  return;
}

int main (int argc, char** argv)
{
    std::cout << "Test Program Started" << std::endl;
    
    RTC::Manager* manager;
    manager = RTC::Manager::init(argc, argv);
    manager->init(argc, argv);
    manager->setModuleInitProc(MyModuleInit);
    manager->activateManager();
    manager->runManager();
    
    std::cout << "Test Program Ended" << std::endl;
    
    return 0;
}

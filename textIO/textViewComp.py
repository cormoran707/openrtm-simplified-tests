#!/usr/bin/env python
# -*- coding: utf-8 -*-

# thid code id based on http://openhri.readthedocs.org/en/latest/tutorial/step3-ja.html

import sys
import time
import OpenRTM_aist
import RTC

textin_spec = ["implementation_id", "Textin",
                  "type_name",         "Textin",
                  "description",       "Console output component",
                  "version",           "1.0",
                  "vendor",            "sample",
                  "category",          "example",
                  "activity_type",     "DataFlowComponent",
                  "max_instance",      "10",
                  "language",          "Python",
                  "lang_type",         "script",
                  ""]
 
class Textin(OpenRTM_aist.DataFlowComponentBase):
    def __init__(self, manager):
        OpenRTM_aist.DataFlowComponentBase.__init__(self, manager)
        self._data = RTC.TimedString(RTC.Time(0,0),"")
        self._inport = OpenRTM_aist.InPort("in", self._data)
 
    def onInitialize(self):
        self.registerInPort("in", self._inport)
        return RTC.RTC_OK
 
    def onExecute(self, ec_id):
        if self._inport.isNew() :
            self._data = self._inport.read();
            sys.stdout.write("in >> " + self._data.data + "\n")
            sys.stdout.flush()

        time.sleep(0.1)
        return RTC.RTC_OK
    
 
def MyModuleInit(manager):
    inprofile = OpenRTM_aist.Properties(defaults_str=textin_spec)
    manager.registerFactory(inprofile,
                            Textin,
                            OpenRTM_aist.Delete)
    manager.createComponent("Textin")

 
def main():
    mgr = OpenRTM_aist.Manager.init(sys.argv)
    mgr.setModuleInitProc(MyModuleInit)
    mgr.activateManager()
    mgr.runManager()
 
if __name__ == "__main__":
    main()

#include <rtm/Manager.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include "ModuleName.h"

#include<opencv2/opencv.hpp>

void MyModuleInit(RTC::Manager* manager)
{
  ModuleNameInit(manager);
  RTC::RtcBase* comp;

  // Create a component
  comp = manager->createComponent("ModuleName");

  if (comp==NULL)
  {
    std::cerr << "Component create failed." << std::endl;
    abort();
  }

  return;
}

int main (int argc, char** argv)
{
    std::cout << "Test Program Started" << std::endl;
    
    RTC::Manager* manager;
    manager = RTC::Manager::init(argc, argv);
    manager->init(argc, argv);
    manager->setModuleInitProc(MyModuleInit);
    manager->activateManager();
    manager->runManager(true);

    std::cout << "press 'q' on the window to end" << std::endl;
    while(true){
        cv::Mat img = cv::Mat::zeros(300,300,CV_8UC3);
        cv::imshow("img", img);
        char key=cv::waitKey(100);
        if(key=='q') break;
    }
    
    std::cout << "Test Program Ended" << std::endl;
    
    return 0;
}

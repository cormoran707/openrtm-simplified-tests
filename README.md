# OpenRTM-Simplified-Tests
OpenRTM simple test projects

- OpenRTM(C++)の必要最低限の部分を抜き出して色々追加し直したもの

- cmake あまり使ったことがなかったのでcmakeに慣れることも兼ねる

- OpenRTM と他のライブラリを一緒に使う際のシンプルな備忘録的サンプル

## 依存ライブラリ

各プロジェクトフォルダ参照

## License

MIT Lisence

#include<iostream>
#include "ModuleName.h"

static const char* modulename_spec[] =
  {
    "implementation_id", "ModuleName",
    "type_name",         "ModuleName",
    "description",       "ModuleDescription",
    "version",           "1.0.0",
    "vendor",            "VenderName",
    "category",          "Category",
    "activity_type",     "PERIODIC",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    ""
  };

ModuleName::ModuleName(RTC::Manager* manager)
  : RTC::DataFlowComponentBase(manager)
{
    std::cout << "ModuleName Contructor" << std::endl;
}

ModuleName::~ModuleName()
{
    std::cout << "ModuleName Destructor" << std::endl;
}

RTC::ReturnCode_t ModuleName::onInitialize()
{
    std::cout << "ModuleName onInitialize" << std::endl;
    return RTC::RTC_OK;
}

extern "C"
{
 
  void ModuleNameInit(RTC::Manager* manager)
  {
    coil::Properties profile(modulename_spec);
    manager->registerFactory(profile,
                             RTC::Create<ModuleName>,
                             RTC::Delete<ModuleName>);
  }
  
};



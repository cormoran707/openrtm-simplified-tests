#!/usr/bin/env python
# -*- coding: utf-8 -*-

# thid code id based on http://openhri.readthedocs.org/en/latest/tutorial/step3-ja.html

import sys
import time
import OpenRTM_aist
import RTC

textinput_spec = ["implementation_id", "Textinput",
                  "type_name",         "Textinput",
                  "description",       "Console input component",
                  "version",           "1.0",
                  "vendor",            "sample",
                  "category",          "example",
                  "activity_type",     "DataFlowComponent",
                  "max_instance",      "10",
                  "language",          "Python",
                  "lang_type",         "script",
                  ""]
 
class Textinput(OpenRTM_aist.DataFlowComponentBase):
    def __init__(self, manager):
        OpenRTM_aist.DataFlowComponentBase.__init__(self, manager)
        self._data = RTC.TimedString(RTC.Time(0,0),"")
        self._outport = OpenRTM_aist.OutPort("out", self._data)
        
    def onInitialize(self):
        self.registerOutPort("out", self._outport)
        return RTC.RTC_OK
 
    def onExecute(self, ec_id):
        sys.stdout.write("input >> ")
        sys.stdout.flush()
        self._data.data = raw_input();
        OpenRTM_aist.setTimestamp(self._data)
        self._outport.write()
        time.sleep(0.2)
        return RTC.RTC_OK

     
def MyModuleInit(manager):
    outprofile = OpenRTM_aist.Properties(defaults_str=textinput_spec)
    manager.registerFactory(outprofile,
                            Textinput,
                            OpenRTM_aist.Delete)
    manager.createComponent("Textinput")

 
def main():
    mgr = OpenRTM_aist.Manager.init(sys.argv)
    mgr.setModuleInitProc(MyModuleInit)
    mgr.activateManager()
    mgr.runManager()
 
if __name__ == "__main__":
    main()

#ifndef MODULENAME_H
#define MODULENAME_H

#include <rtm/idl/BasicDataTypeSkel.h>
#include <rtm/idl/ExtendedDataTypesSkel.h>
#include <rtm/idl/InterfaceDataTypesSkel.h>

using namespace RTC;

#include <rtm/Manager.h>
#include <rtm/DataFlowComponentBase.h>
#include <rtm/CorbaPort.h>
#include <rtm/DataInPort.h>
#include <rtm/DataOutPort.h>

class ModuleName
  : public RTC::DataFlowComponentBase
{
 public:
  ModuleName(RTC::Manager* manager);
  ~ModuleName();
   virtual RTC::ReturnCode_t onInitialize();
    
 protected:

 private:

};


extern "C"
{
  DLL_EXPORT void ModuleNameInit(RTC::Manager* manager);
};

#endif // MODULENAME_H
